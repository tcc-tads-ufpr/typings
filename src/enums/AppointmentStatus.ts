export enum AppointmentStatus {
  Scheduled = 'scheduled',
  Canceled = 'canceled',
  Rescheduled = 'rescheduled',
  Done = 'done'
}
