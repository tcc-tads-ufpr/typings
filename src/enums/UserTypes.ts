export enum UserTypes {
  Admin = 'Admin',
  Doctor = 'doctor',
  Patient = 'patient',
  Employee = 'employee'
}
