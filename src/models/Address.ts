import { BaseModel } from './BaseModel';

export interface Address extends BaseModel {
  zipCode?: string;
  street?: string;
  number?: number;
  complement?: string;
  state?: string;
  city?: string;
  neighborhood?: string;
}
