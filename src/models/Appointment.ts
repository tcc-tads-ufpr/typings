import { BaseModel } from './BaseModel';
import { User } from './User';
import { AppointmentStatus } from '../enums';
import { AppointmentType } from './AppointmentType';

export interface Appointment extends BaseModel {
  doctor?: string | User;
  patient?: string | User;
  scheduledBy?: string | User;
  scheduledDate?: string | Date;
  status?: AppointmentStatus;
  rescheduledDate?: string | Date;
  type?: string | AppointmentType;
}
