import { BaseModel } from './BaseModel';

export interface AppointmentType extends BaseModel {
  name?: string;
}
