export interface BaseModel {
  _id?: string;
  readonly createdAt?: string | Date;
  readonly updatedAt?: string | Date;
}
