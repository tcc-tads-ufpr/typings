import { BaseModel } from './BaseModel';
import { Address } from './Address';

export interface Establishment extends BaseModel {
  name?: string;
  address?: string | Address;
}
