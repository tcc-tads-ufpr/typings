import { BaseModel } from './BaseModel';
import { UserTypes } from '../enums';
import { Establishment } from './Establishment';
import { Address } from './Address';

export interface User extends BaseModel {
  name?: string;
  email?: string;
  password?: string;
  type?: UserTypes;
  cpf?: string;
  birthDate?: string | Date;
  crm?: string;
  establishment?: string | Establishment;
  address?: string | Address;
}
